async function downloadImg() {
  let self = this;
  let urlParams;
  (window.onpopstate = function () {
    // get query from url
    var match,
      pl = /\+/g, // Regex for replacing addition symbol with a space
      search = /([^&=]+)=?([^&]*)/g,
      decode = function (s) {
        return decodeURIComponent(s.replace(pl, " "));
      },
      query = window.location.search.substring(1);

    urlParams = {};
    while ((match = search.exec(query)))
      urlParams[decode(match[1])] = decode(match[2]);
  })();

  // get link from query
  let url = urlParams.link || "";

  if (url) {
    var pages = [],
      heights = [],
      width = 0,
      height = 0,
      currentPage = 1;
    var scale = 1.5;

    // Get blob file pdf từ link file pdf
    pdfjsLib.GlobalWorkerOptions.workerSrc = "/mylibs/pdf/pdf.worker.js";
    var loadingTask = pdfjsLib.getDocument(url);
    loadingTask.promise.then(function (pdf) {
      getPage();
      function getPage() {
        pdf.getPage(currentPage).then(function (page) {
          console.log("Printing:" + currentPage);
          var viewport = page.getViewport({ scale });
          var canvas = document.createElement("canvas"),
            ctx = canvas.getContext("2d");
          var renderContext = {
            canvasContext: ctx,
            viewport: viewport,
          };

          canvas.height = viewport.height;
          canvas.width = viewport.width;

          const mypage = page.render(renderContext);
          mypage.promise.then(function () {
            pages.push(ctx.getImageData(0, 0, canvas.width, canvas.height));

            heights.push(height);
            height += canvas.height;
            if (width < canvas.width) width = canvas.width;

            if (currentPage < pdf.numPages) {
              currentPage++;
              getPage();
            } else {
              draw();
            }
          });
        });
      }
    });

    // Tạo ảnh
    function draw() {
      // Vẽ lại ảnh từ PDF
      var canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d");
      canvas.style.display = "none";
      canvas.width = width;
      canvas.height = height;
      for (var i = 0; i < pages.length; i++)
        ctx.putImageData(pages[i], 0, heights[i]);
      document.body.appendChild(canvas);
      var img = new Image();
      img.src = canvas.toDataURL();
      function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(","),
          mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[1]),
          n = bstr.length,
          u8arr = new Uint8Array(n);

        while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
        }

        return new File([u8arr], filename, { type: mime });
      }

      // Tải ảnh
      var file = dataURLtoFile(img.src, "hello.img");
      let urlImage = URL.createObjectURL(file);
      let day = `${new Date().getDate()}`;
      let month = `${new Date().getMonth() + 1}`;
      let year = `${new Date().getFullYear()}`;

      // Tạo thẻ a để download
      var a = document.createElement("a");
      a.setAttribute("download", `${year}-${month}-${day}-BanTheHienLenh`);
      a.setAttribute("href", urlImage);
      document.body.appendChild(a);
      let preivew = document.getElementById("previewPdf");
      let preivewImage = document.getElementById("previewPdfImage");
      preivewImage.src = urlImage;
      preivew.style.display = "block";
      let btn = document.getElementById("btn-download");
      btn.style.display = "flex";
      document.body.removeChild(a);
      btn.addEventListener("click", function () {
        a.click();
      });

      
    }
  }
}
window.onload = function () {
  downloadImg();
};
